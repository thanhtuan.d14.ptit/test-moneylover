import {Action} from "../utils/constant";

export const saveData =(data)=>{

    document.cookie = 'data='+JSON.stringify(data)+"endData";
};

export const readData =()=>{
    const cookie = document.cookie;
    if(cookie.includes("data")){
        const data = cookie.substring(cookie.indexOf("data=") + 5, cookie.indexOf("endData"));
        return JSON.parse(data);
    }

    return [];
};

export function saveToDo() {
    return {
        type: Action.SAVE_TODO
    }
}

export function openDetail() {
    return {
        type: Action.OPEN_DETAIL
    }
}