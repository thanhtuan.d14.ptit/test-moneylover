import React from 'react'
import {Button, Form, Header, Input} from "semantic-ui-react";
import {readData, saveData, saveToDo} from "../actions";
import {connect} from "react-redux";

class CreateToDo extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            name: "",
            description:""
        };
        this.changeData = this.changeData.bind(this);
        this.saveData = this.saveData.bind(this);
    }

    changeData(evt,{placeholder, value}){
        if(placeholder==="name"){
            this.setState({name:value});
        }else {
            this.setState({description:value});
        }
    }

    saveData(){
        const {name, description} = this.state;
        let data = readData();
        const x = {name:name, description:description,check:false};
        data.push(x);

        saveData(data);
        this.setState({name:"", description:""});
        this.props.dispatch(saveToDo());
    }

    render() {
        const {name, description} = this.state;
        return(
            <div>
                <Header as="h4" style={{marginTop:'2em'}}>Create ToDo</Header>
                <Form className="form_create">
                    <Form.Field >
                        <label>Name</label>
                        <Input placeholder='name' value={name} onChange={this.changeData} />
                    </Form.Field>
                    <Form.Field>
                        <label>Description</label>
                        <Input placeholder='description' value={description} onChange={this.changeData} />
                    </Form.Field>
                    <Button onClick={this.saveData}>Save</Button>
                </Form>
            </div>
        )
    }
}

export default connect()(CreateToDo);