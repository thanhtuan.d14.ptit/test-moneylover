import React from 'react'
import {Button, Checkbox, Label, Table} from "semantic-ui-react";
import {openDetail, readData, saveData} from "../actions";
import {connect} from "react-redux";
import {Action} from "../utils/constant";

class ListToDo extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data:[]
        };
        this.reloadData = this.reloadData.bind(this);
        this.clickView = this.clickView.bind(this);
    }

    componentDidMount() {
        this.reloadData();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.todo === Action.SAVE_TODO)
            this.reloadData();
    }

    reloadData(){
        const data = readData();
        this.setState({data});
    }

    clickView(){

        this.props.dispatch(openDetail());
    }

    clickDelete(index){
        let {data} = this.state;
        data.splice(index,1);
        saveData(data);
        this.reloadData();
    }

    render() {
        const  {data} = this.state;
        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Check</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Action</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {data.map((dt,index)=>{
                        return(
                            <Table.Row>
                                <Table.Cell><Checkbox checked={dt.check}/></Table.Cell>
                                <Table.Cell>{dt.name}</Table.Cell>
                                <Table.Cell>
                                    <Button onClick={this.clickView}>View</Button>
                                    <Button onClick={()=>this.clickDelete(index)}>Delete</Button>
                                </Table.Cell>
                            </Table.Row>
                        )
                })}
                </Table.Body>
            </Table>
        )
    }
}

function mapPropsToState(props, state){
        state = props;
        return state;
}

export default connect(mapPropsToState)(ListToDo);