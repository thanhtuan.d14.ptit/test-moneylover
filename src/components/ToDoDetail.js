import React from 'react';
import {Button, Dimmer, Form, Input} from "semantic-ui-react";
import {connect} from "react-redux";
import {Action} from "../utils/constant";


class ToDoDetail extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            active:false,
            name: "",
            description:""
        };

        this.handleClose = this.handleClose.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.todo === Action.OPEN_DETAIL)
            this.setState({active: true})
    }

    handleClose(){
        this.setState({active:false})
    }

    render() {
        return(
            <Dimmer active={active} onClickOutside={this.handleClose} page>
                <Form className="form_create">
                    <Form.Field >
                        <label>Name</label>
                        <Input placeholder='name' value={name} onChange={this.changeData} />
                    </Form.Field>
                    <Form.Field>
                        <label>Description</label>
                        <Input placeholder='description' value={description} onChange={this.changeData} />
                    </Form.Field>
                    <Button onClick={this.saveData}>Save</Button>
                </Form>

            </Dimmer>
        )
    }

}
function mapPropsToState(props, state){
    state = props;
    return state;
}

export default connect(mapPropsToState)(ToDoDetail);