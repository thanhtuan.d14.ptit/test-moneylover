import React from 'react';
import CreateToDo from "../components/CreateToDo";
import '../style/main.css'
import ListToDo from "../components/ListToDo";
class Main extends React.Component{

    render(){
        return(
            <div>
                <CreateToDo/>
                <ListToDo/>
            </div>
        )
    }
}

export default Main;