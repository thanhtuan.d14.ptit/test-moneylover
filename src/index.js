import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import Main from "./containers/Main";
import {applyMiddleware, createStore} from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers';
import thunk from "redux-thunk";
import {createLogger} from "redux-logger";


const middleware = [ thunk ];
middleware.push(createLogger());

const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);

ReactDOM.render(
    <Provider store={store}>
        <Main />
    </Provider>
    , document.getElementById('root')
);

serviceWorker.unregister();
