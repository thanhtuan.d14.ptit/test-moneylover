import {combineReducers} from "redux";
import {Action} from "../utils/constant";

function todo(state = {}, action) {
    switch (action.type) {
        case Action.SAVE_TODO: {
            return Action.SAVE_TODO;
        }
        case Action.OPEN_DETAIL:{
            return Action.OPEN_DETAIL;
        }

        default:
            return "";
    }
}

export default combineReducers({
    todo
})